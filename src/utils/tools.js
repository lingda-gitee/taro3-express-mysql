import Taro from "@tarojs/taro";
import { objToString } from "./utils";

const tools = {
  apiRequest: (options) => {
    const { url = "", params = {}, method = "GET", ...rest } = options;

    return new Promise((resolve, reject) => {
      Taro.request({
        url,
        data: params,
        method,
        ...rest,
      })
        .then((res) => {
          const { data } = res;
          if (data?.code === 1) {
            resolve(data);
          } else {
            reject(res);
          }
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  showLoading: (param = "") => {
    let dptOpts = {
      title: "加载中",
      mask: true, // 防止触摸穿透
    };

    if (Object.prototype.toString.call(param) === "[object String]") {
      dptOpts.title = param;
    } else if (Object.prototype.toString.call(param) === "[object Object]") {
      dptOpts = {
        ...dptOpts,
        ...param,
      };
    } else {
      throw new Error("参数类型有误, 应为字符串或对象");
    }

    return Taro.showLoading(dptOpts);
  },
  hideLoading: () => {
    Taro.hideLoading();
  },
  showToast: (param) => {
    let dptOpts = {
      title: "温馨提示",
      icon: "none",
      mask: true,
      duration: 2000,
    };

    if (Object.prototype.toString.call(param) === "[object String]") {
      dptOpts.title = param;
    } else if (Object.prototype.toString.call(param) === "[object Object]") {
      dptOpts = {
        ...dptOpts,
        ...param,
      };
    } else {
      throw new Error("参数类型有误, 应为字符串或对象");
    }

    return Taro.showToast(dptOpts);
  },
  /**
   *
   * @param url 页面路径
   * @param data 页面参数
   */
  navigateTo: ({ url, data }) => {
    const searchStr = objToString(data);

    return Taro.navigateTo({
      url: `${url}?${searchStr}`,
    });
  },
  /**
   *
   * @param {缓存值的key} key
   * @param {缓存值的value} value
   * @param {持续时间/秒} expires
   */
  setStorageSyncExpires: (key, value, expires) => {
    try {
      const beginTime = Date.now();

      Taro.setStorageSync(key, {
        [key]: value,
        beginTime,
        expires: expires * 1000,
      });
    } catch (err) {
      tools.showToast("设置缓存失败");
    }
  },
  getStorageSyncExpires: (key) => {
    try {
      const curTime = Date.now();

      console.log("读取前");
      const data = Taro.getStorageSync(key);
      console.log("读取后data: ", data, curTime);

      const { beginTime, expires } = data;
      console.log("解构后: ", beginTime, expires);

      if (curTime - beginTime >= expires) {
        Taro.removeStorageSync(key);

        console.log("删除了userInfo");
        return null;
      }

      // beginTime = curTime;
      tools.setStorageSyncExpires(key, data[key], expires);

      console.log("userInfo1", data[key]);
      return data[key];
    } catch (err) {
      tools.showToast("读取缓存失败");
    }
  },
  /**
   *
   * @param {cb} 如果登陆了, 再正常执行的回调
   * @mean 触发事件时用于判断是否登陆的方法
   */
  doLogin: (cb) => {
    const userInfo = tools.getStorageSyncExpires("userInfo");

    console.log("userInfo: ", userInfo);

    if (!userInfo.userPhone) {
      tools.navigateTo({
        path: "/pages/login/login",
      });
    } else {
      cb?.();
    }
  },
  isAliPay: Taro.ENV_TYPE.ALIPAY === Taro.getEnv(),
  isH5: Taro.ENV_TYPE.WEB === Taro.getEnv(),
};

export default tools;
