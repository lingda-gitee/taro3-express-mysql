export const objToString = (obj) => {
  let arr = [];
  if (
    Object.prototype.toString.call(obj) === "[object Object]" &&
    Object.keys(obj).length
  ) {
    for (let k in obj) {
      arr.push(`${k}=${obj[k]}`);
    }
  }

  return arr.join("&");
};

export const timestampToWeek = (timestamp) => {
  let d = new Date(timestamp);
  let week = d.getDay();

  let str;

  switch (week) {
    case 0:
      str = "星期日";

      break;

    case 1:
      str = "星期一";

      break;

    case 2:
      str = "星期二";

      break;

    case 3:
      str = "星期三";

      break;

    case 4:
      str = "星期四";

      break;

    case 5:
      str = "星期五";

      break;

    case 6:
      str = "星期六";

      break;
  }

  return str;
};

/**
 *
 * @param dateStr 2023-04-02
 * @return 4月2日
 */
export const formatDateStr = (dateStr) => {
  if (!dateStr) {
    return "";
  }
  const strArr = dateStr.split("-").slice(1);

  let month = strArr[0],
    day = strArr[1];

  let _month, _day;

  _month = month[0] === "0" ? month[1] : month;
  _day = day[0] === "0" ? day[1] : day;

  return `${_month}月${_day}日`;
};

// 函数防抖
export const debounce = (fn, delay, triggleFirst) => {
  let timer = null;

  return function (...args) {
    if (timer) {
      clearTimeout(timer);
    }

    if (triggleFirst) {
      let exec = !timer;

      timer = setTimeout(() => {
        timer = null;
      }, delay);

      if (exec) {
        console.log("触发");
        fn.apply(this, args);
      }
    } else {
      timer = setTimeout(() => {
        fn.apply(this, agrs);
      }, delay);
    }
  };
};
