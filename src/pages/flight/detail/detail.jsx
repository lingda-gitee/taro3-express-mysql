import { PureComponent } from "react";
import { View, Text, Input, Button } from "@tarojs/components";
import "./detail.scss";

import Taro, { getCurrentInstance } from "@tarojs/taro";
import dayjs from "dayjs";
import { formatDateStr } from "@/utils/utils";

import { AtButton } from "taro-ui";
import withShare from "@/common/decorators/withShare";
import LoginDecorator from "@/components/LoginDecorator";
import { connect } from "react-redux";
import tools from "@/utils/tools";
import { orderRequest } from "@/common/api";
import { ERR_MSG } from "@/common/constant";

@withShare({
  title: "快来分享给好友吧",
  imageUrl:
    "https://img0.baidu.com/it/u=3423209198,349864538&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=278",
})
@LoginDecorator
@connect(({ user }) => {
  return {
    user,
  };
})
export default class Detail extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedFlightData: {},
    };
  }

  handlerOrder() {
    const { userPhone } = this.props.user;
    const { selectedFlightData } = this.state;

    // 先判断是否登录, 如果登录了再把要做的事情(创建订单)放到cb中
    tools.doLogin(() => {
      tools.showLoading();

      orderRequest({
        userPhone,
        orderInfo: selectedFlightData,
      })
        .then((res) => {
          console.log("订单: ", res);
          tools
            .showToast({
              title: "预定成功",
              icon: "loading",
              duration: 2000,
            })
            .then(() => {
              Taro.switchTab({
                url: "/pages/order/order",
              });
            });
        })
        .catch((err) => {
          tools.showToast(err?.data?.message || ERR_MSG);
        })
        .finally(() => {
          tools.hideLoading();
        });
    });
  }

  componentDidMount() {
    const { params } = getCurrentInstance().router;
    this.setState({
      selectedFlightData: {
        ...params,
      },
    });
  }

  render() {
    const { selectedFlightData } = this.state;
    const {
      arrAirportName,
      arrTimeStr,
      dptAirportName,
      dptTime,
      dptTimeStr,
      price,
      curDateStr,
    } = selectedFlightData;

    console.log("cur: ", curDateStr);

    const { isLogin, userPhone, nickName } = this.props.user;

    return (
      <View className="order-detail">
        <View className="flight-info">
          <View className="company-time">
            <Text className="fly-type">直飞</Text>
            <Text className="company">🇨🇳 中国国航</Text>
            <Text className="time">{formatDateStr(curDateStr)}</Text>
          </View>
          <View className="dpt-arr-time">{`${dptTimeStr} ----------- ${arrTimeStr}`}</View>
          <View className="dpt-arr-airport">{`${dptAirportName} ------------------- ${arrAirportName}`}</View>
        </View>
        <View className="custom-info">
          <View className="custom">乘机人</View>
          {isLogin ? (
            <View className="name">{nickName}</View>
          ) : (
            <AtButton
              type="secondary"
              size="normal"
              className="add"
              onClick={tools.doLogin}
            >
              新增
            </AtButton>
          )}
        </View>
        <View className="cellphone">
          <View className="phone">联系手机</View>
          <View className="input-container">
            <Text className="pre">+86</Text>
            <Input
              className="input"
              placeholder="请输入乘机人手机号"
              disabled={userPhone ? true : false}
              value={userPhone}
            ></Input>
          </View>
        </View>
        <View className="price-sub">
          <View className="price">{`¥ ${price}`}</View>
          <View className="sub" onClick={this.handlerOrder.bind(this)}>
            订
          </View>
        </View>
        {/* openType=share 类型点击会触发onShareAppMessage方法 */}
        <Button openType="share" className="share-btn">
          分享给好友
        </Button>
        <View className="advertise">
          <View className="ad1">金牌服务-出行保障</View>
          <View className="ad2">
            <Text>低价保障</Text>
            <Text>隐私保障</Text>
            <Text>退改无忧</Text>
          </View>
        </View>
      </View>
    );
  }
}
