import { PureComponent } from "react";
import {
  View,
  Text,
  SwiperItem,
  Swiper,
  Image,
  Button,
} from "@tarojs/components";
import "./index.scss";
import Taro from "@tarojs/taro";
import { AtButton } from "taro-ui";
import dayjs from "dayjs";

import Tab from "@/components/Tab/index";

import { adsRequest } from "@/common/api";
import { connect } from "react-redux";
import tools from "@/utils/tools";

const FLIGHT_TABS = [
  {
    id: 0,
    label: "单程",
  },
  {
    id: 1,
    label: "多程",
  },
  {
    id: 2,
    label: "往返",
  },
];

@connect(({ flightIndex }) => {
  return {
    flightIndex,
  };
})
export default class FlightIndex extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      adsList: [],
    };
  }

  onTabClick(id) {
    console.log("id: ", id);
  }

  getAdsData() {
    adsRequest().then((res) => {
      console.log("res: ", res);
      const { data = [], message = "" } = res;
      this.setState({
        adsList: data.slice(10, 12),
      });
    });
  }

  chooseFlightCity(type) {
    this.props.dispatch({
      type: "flightIndex/updateState",
      payload: {
        cityType: type,
      },
    });

    Taro.navigateTo({
      url: "/pages/airportList/airportList",
    });
  }

  chooseFlightDate() {
    Taro.navigateTo({
      url: "/pages/calendar/calendar",
    });
  }

  // 获取经纬度
  getLocationInfo() {
    Taro.getLocation({
      type: "gcj02",
    })
      .then((res) => {
        const { latitude, longitude } = res;
        this.getCity({ latitude, longitude });
      })
      .catch((err) => {
        tools.showToast("获取地理位置失败");
      });
  }

  // 获取城市定位
  getCity({ latitude, longitude }) {
    Taro.request({
      url: `https://apis.map.qq.com/ws/geocoder/v1/?key=JKLBZ-WN3K4-HFSU6-DB5UU-2FGCS-CLB4J&location=${latitude},${longitude}`,
    })
      .then((res) => {
        console.log("定位信息: ", res);
        const { data } = res;
        const cityInfo = data?.result?.ad_info || {};
        this.props.dispatch({
          type: "flightIndex/updateState",
          payload: {
            dptCityId: cityInfo.city_code || 2,
            dptCityName: cityInfo.city || "上海",
          },
        });
      })
      .catch((err) => {
        tools.showToast("获取地理位置失败");
      });
  }

  linkToList() {
    const {
      dptCityId,
      dptCityName,
      dptDate,
      dptAirportName,
      arrCityId,
      arrCityName,
      arrAirportName,
    } = this.props.flightIndex;

    tools.navigateTo({
      url: "/pages/flight/list/list",
      data: {
        dptCityId,
        dptCityName,
        dptDate,
        dptAirportName,
        arrCityId,
        arrCityName,
        arrAirportName,
      },
    });
  }

  async componentDidMount() {
    await this.getAdsData();
    await this.getLocationInfo();
  }

  render() {
    const { adsList } = this.state;

    const { arrCityName, dptCityName, dptDate } = this.props.flightIndex;
    console.log("始末: ", dptCityName, arrCityName);

    return (
      <View className="flight-container">
        <View className="flight-top">
          <Tab
            tabList={FLIGHT_TABS}
            onTabClick={this.onTabClick.bind(this)}
            className="flight-index-tab"
          >
            <SwiperItem>
              <View className="item station">
                <View
                  className="cell from"
                  onClick={() => this.chooseFlightCity("depart")}
                >
                  {dptCityName}
                </View>
                <Text className="iconfont icon-zhihuan">---</Text>
                <View
                  className="cell to"
                  onClick={() => this.chooseFlightCity("arrive")}
                >
                  {arrCityName}
                </View>
              </View>
              <View className="date" onClick={this.chooseFlightDate.bind(this)}>
                {dayjs(dptDate).format("M月D日")}
              </View>
              <AtButton
                type="primary"
                size="normal"
                circle
                className="btn-search"
                onClick={this.linkToList.bind(this)}
              >
                机票查询
              </AtButton>
            </SwiperItem>
          </Tab>
        </View>
        {/* 广告图片部分 */}
        <Swiper className="advs-banner-bd" autoplay circular interval={3000}>
          {adsList &&
            adsList.map((item) => {
              return (
                <SwiperItem key={item.id} className="banner-item">
                  <Image className="img" src={item.imgUrl}></Image>
                </SwiperItem>
              );
            })}
        </Swiper>
      </View>
    );
  }
}
