import { PureComponent } from "react";
import {
  View,
  Text,
  ScrollView,
  Image,
  Picker,
  Block,
} from "@tarojs/components";
import "./list.scss";

import Taro, { getCurrentInstance } from "@tarojs/taro";
import dayjs from "dayjs";
import { MIN_DATE, MAX_DATE, ERR_MSG } from "@/common/constant";
import { timestampToWeek } from "@/utils/utils";
import Skeleton from "taro-skeleton";
import "taro-skeleton/dist/index.css";
import VirtualList from "@/components/VirtualList";

import { flightListRequest } from "@/common/api";
import tools from "@/utils/tools";

export default class FlightList extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dateList: this.formatDateList(),
      flightData: {}, // 航班参数
      flightList: [], // 返回的航班数据
      flightCompanylist: ["中国国航"], //航司列表
      curFlightCompanyIndex: 0, //选中的航司下表
      scrollTop: "",
      curDateStr: "",
    };
    this.initFlightList = [];
  }

  formatDateList() {
    let minStr = dayjs(MIN_DATE).valueOf();
    const maxStr = dayjs(MAX_DATE).valueOf(); // 时间戳
    const dayStr = 1000 * 60 * 60 * 24; // 一天
    let res = [];

    for (; minStr <= maxStr; minStr += dayStr) {
      res.push({
        dateStr: dayjs(minStr).format("YYYY-MM-DD"), // 2023-6-10
        day: dayjs(minStr).format("M-DD"), // 6-10
        week: timestampToWeek(minStr), // 周四
      });
    }

    return res;
  }

  getFlightList() {
    const { flightData } = this.state;
    console.log("flightData: ", flightData);

    tools.showLoading();

    this.setState({
      scrollTop: "",
    });

    flightListRequest({
      ...flightData,
    })
      .then((res) => {
        console.log("航班列表: ", res);

        // const flightCompanyArr = res.data?.map(item => {
        //   return item.airCompanyName
        // });

        this.setState({
          flightList: res.data,
          scrollTop: 0, // 回到顶部
          // flightCompanylist: [ ...new Set(flightCompanyArr) ]
        });
      })
      .catch((err) => {
        tools.showToast(ERR_MSG);
      })
      .finally(() => {
        tools.hideLoading();
      });
  }

  handleFlightCompanyClick(e) {
    this.setState({
      curFlightCompanyIndex: e.detail.value,
      scrollTop: "",
    });

    // 选择航司后再请求接口, 再把scrollTop设为0
  }

  listToOrderDetail(flightInfo) {
    const { curDateStr } = this.state;
    tools.navigateTo({
      url: "/pages/flight/detail/detail",
      data: {
        ...flightInfo,
        curDateStr,
      },
    });
  }

  chooseDate = (dateStr) => {
    this.setState(
      {
        curDateStr: dateStr,
      },
      () => {
        this.getFlightList();
      },
    );
  };

  async componentDidMount() {
    const { params } = getCurrentInstance().router;
    console.log("params: ", params);

    const {
      dptCityId,
      dptCityName,
      dptDate,
      dptAirportName,
      arrCityId,
      arrCityName,
      arrAirportName,
    } = params;

    await this.setState({
      flightData: {
        dptCityId,
        dptCityName,
        dptDate,
        dptAirportName,
        arrCityId,
        arrCityName,
        arrAirportName,
      },
      curDateStr: dptDate,
    });

    this.getFlightList();

    Taro.setNavigationBarTitle({
      title: `${dptCityName} - ${arrCityName}`,
    });
  }

  handleRender = (flight, index) => {
    const { dptAirportName, dptTimeStr, arrTimeStr, arrAirportName, price } =
      flight;

    return (
      <View
        className="content-item"
        key={item.id}
        onClick={this.listToOrderDetail.bind(this, item)}
      >
        <View className="item-main">
          <View className="dpt-arr-time">{`${item.dptTimeStr} ---------- ${item.arrTimeStr}`}</View>
          <View className="dpt-arr-airport">{`${item.dptAirportName} ---------- ${item.arrAirportName}`}</View>
          <View className="company">🇫🇯 中国国航</View>
        </View>
        <View className="item-price">{`¥ ${item.price}`}</View>
      </View>
    );
  };

  render() {
    const {
      dateList,
      flightData,
      flightList,
      flightCompanylist,
      curFlightCompanyIndex,
      scrollTop,
      curDateStr,
    } = this.state;

    const { dptDate } = flightData;

    return (
      <View className="flight-list">
        <View className="calendar-list">
          {/* scrollIntoView为锚点id */}
          <ScrollView
            className="calendar-scroll-list"
            scrollX
            scrollWithAnimation
            scrollIntoView={`date-${dptDate}`}
            scrollTop={scrollTop}
          >
            <View className="flex-box">
              {dateList?.map((date) => {
                return (
                  <View
                    key={date.dateStr}
                    className={[
                      "item",
                      date.dateStr === curDateStr ? " active" : "",
                    ].join("")}
                    id={`date-${date.dateStr}`}
                    onClick={this.chooseDate.bind(this, date.dateStr)}
                  >
                    <View className="date">{date.day}</View>
                    <View className="week">{date.week}</View>
                  </View>
                );
              })}
            </View>
          </ScrollView>
        </View>
        {flightList.length > 0 ? (
          <View className="flight-content">
            {/* <VirtualList
              list={flightList}
              onRender={this.handleRender}
              className=""
            ></VirtualList> */}

            <ScrollView scrollY scrollWithAnimation>
              {flightList.map((item, index) => {
                return (
                  <View
                    className="content-item"
                    key={item.id}
                    onClick={this.listToOrderDetail.bind(this, item)}
                  >
                    <View className="item-main">
                      <View className="dpt-arr-time">{`${item.dptTimeStr} ---------- ${item.arrTimeStr}`}</View>
                      <View className="dpt-arr-airport">{`${item.dptAirportName} ---------- ${item.arrAirportName}`}</View>
                      <View className="company">🇫🇯 中国国航</View>
                    </View>
                    <View className="item-price">{`¥ ${item.price}`}</View>
                  </View>
                );
              })}
            </ScrollView>
          </View>
        ) : (
          Array(7)
            .fill(0)
            .map((item, index) => {
              return (
                <Skeleton key={index} row={10} action rowHeight={34}></Skeleton>
              );
            })
        )}
        <View className="picker">
          <Picker
            range={flightCompanylist}
            value={curFlightCompanyIndex}
            onChange={this.handleFlightCompanyClick.bind(this)}
          >
            筛选
          </Picker>
        </View>
      </View>
    );
  }
}
