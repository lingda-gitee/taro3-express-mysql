import { Component } from "react";
import { View, Text } from "@tarojs/components";
import "./index.scss";

import FlightIndex from "../flight/index/index";

const DEFAULT_TAB_LIST = [
  { title: "机票", tab: "flight", index: 0 },
  { title: "火车票", tab: "train", index: 1 },
  { title: "酒店", tab: "hotel", index: 2 },
  { title: "汽车票", tab: "bus", index: 3 },
];

export default class Index extends Component {
  constructor(props) {
    super(props);
    this.state = {
      curTabIdx: 0,
    };
  }

  handleTabClick(index) {
    this.setState({
      curTabIdx: index,
    });
  }

  render() {
    const { curTabIdx } = this.state;
    const innerStyle = {
      width: `${100 / DEFAULT_TAB_LIST.length}%`,
      transform: `translateX(${curTabIdx * 100}%)`,
    };

    return (
      <View className="index-container">
        <View className="top">
          <View className="index-tab">
            {DEFAULT_TAB_LIST &&
              DEFAULT_TAB_LIST.map((item) => {
                return (
                  <View
                    key={item.tab}
                    className={[
                      "index-tab-item",
                      ` ${item.tab}`,
                      curTabIdx === item.index ? " active" : "", // 继承类的时候, 后面的类要有个空格前缀
                    ].join("")}
                    onClick={this.handleTabClick.bind(this, item.index)}
                  >
                    <Text className="index-tab-item-title">{item.title}</Text>
                  </View>
                );
              })}
          </View>
        </View>

        {DEFAULT_TAB_LIST[curTabIdx]["tab"] === "flight" ? (
          <FlightIndex />
        ) : (
          <>敬请期待 !</>
        )}
      </View>
    );
  }
}
