import { Component } from "react";
import { View, Text, Button, SwiperItem, ScrollView } from "@tarojs/components";
import "./order.scss";

import tools from "@/utils/tools";
import LoginDecorator from "@/components/LoginDecorator";
import { connect } from "react-redux";
import { AtButton } from "taro-ui";
import Taro, { eventCenter, getCurrentInstance } from "@tarojs/taro";
import Tab from "@/components/Tab";
import { getOrderListRequest } from "@/common/api";
import { ERR_MSG } from "@/common/constant";
import dayjs from "dayjs";

const TAB_LIST = [
  {
    label: "机票",
    id: 0,
  },
  {
    label: "火车票",
    id: 1,
  },
  {
    label: "酒店",
    id: 2,
  },
  {
    label: "汽车票",
    id: 3,
  },
];

@LoginDecorator
@connect(({ user }) => {
  return {
    ...user,
  };
})
export default class Order extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orderList: [],
      isRefresh: false,
    };
  }

  handleLogout() {
    try {
      Taro.removeStorageSync("userInfo");

      this.props.dispatch({
        type: "user/logout",
      });

      tools.showToast("退出登录成功");
    } catch (err) {
      tools.showToast("退出登录失败");
    }
  }

  getOrderList() {
    tools.showLoading();

    const userPhone =
      this.props.userPhone ||
      tools.getStorageSyncExpires("userInfo")?.userInfo?.userPhone;

    getOrderListRequest({
      userPhone,
    })
      .then((res) => {
        console.log("订单列表: ", res);

        this.setState({
          orderList: res.data,
        });
      })
      .catch((err) => {
        tools.showToast(err?.data?.message || ERR_MSG);
      })
      .finally(() => {
        tools.hideLoading();

        this.setState({
          isRefresh: false,
        });
      });
  }

  handlePullDownRefresh = () => {
    this.setState(
      {
        isRefresh: true,
      },
      this.getOrderList(),
    );
  };

  renderList() {
    const { orderList, isRefresh } = this.state;

    return (
      <ScrollView
        scrollY
        style={{ height: "100%" }}
        className="order-list-box"
        refresherEnabled
        refresherTriggered={isRefresh}
        onRefresherRefresh={this.handlePullDownRefresh}
      >
        {orderList?.map((order) => {
          const { dptCityName, arrCityName, dptTime, dptTimeStr, price } =
            order;
          console.log("order: ", order);

          return (
            <View className="item">
              <View className="left">
                <Text className="dpt-city">{dptCityName}--</Text>
                <Text className="arr-city">{arrCityName}</Text>
                <View className="dpt-time">{`${dayjs(dptTime).format(
                  "YYYY-MM-DD",
                )} ${dptTimeStr}`}</View>
              </View>
              <View className="right">¥ {price}</View>
            </View>
          );
        })}
      </ScrollView>
    );
  }

  $instance = getCurrentInstance();

  // 从订单详情页跳到订单页时重新请求
  onShowDIY = () => {
    if (this.props.isLogin) {
      this.getOrderList();
    }
  };

  toLoginPage = () => {
    Taro.navigateTo({
      url: "/pages/login/login",
    });
  };

  componentDidMount() {
    // await this.getOrderList();

    const onShowEventId = this.$instance.router.onShow; // 获取小程序的onShow方法
    eventCenter.on(onShowEventId, this.onShowDIY); // 监听并执行自定义的onShow, 因为高阶组件的onShow方法失效
  }

  render() {
    const { isLogin, nickName } = this.props;

    {
      return isLogin ? (
        <View className="order-container">
          <View className="user">
            <Text className="user-name">欢迎, {nickName || "--"}</Text>
            <Text className="logout" onClick={this.handleLogout.bind(this)}>
              退出
            </Text>
          </View>
          <Tab
            tabList={TAB_LIST}
            isOrder={true}
            className="tab"
            onTabClick={() => {}}
          >
            {TAB_LIST.map((item) => {
              return (
                <SwiperItem key={item.id}>
                  {item.id === 0 ? (
                    // 渲染机票列表
                    this.renderList()
                  ) : (
                    <View className="no-content">暂无数据</View>
                  )}
                </SwiperItem>
              );
            })}
          </Tab>
        </View>
      ) : (
        <View className="logout-status">
          <Text className="check-order">登录查看订单</Text>
          <AtButton
            type="primary"
            size="normal"
            className="login-now"
            onClick={this.toLoginPage.bind(this)}
          >
            立即登陆
          </AtButton>
        </View>
      );
    }
  }
}
