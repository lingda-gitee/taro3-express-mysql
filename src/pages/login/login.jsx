import { PureComponent } from "react";
import { View, Text, Input } from "@tarojs/components";
import "./login.scss";
import { AtButton } from "taro-ui";

import { debounce } from "@/utils/utils";
import { loginRequest } from "@/common/api";
import tools from "@/utils/tools";

import { connect } from "react-redux";
import Taro from "@tarojs/taro";

@connect(({ user }) => {
  return {
    user,
  };
})
export default class Login extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      nickName: "",
      userPhone: "",
      password: "",
    };
  }

  // onInput事件每输入一个字符都会触发, 做防抖限制触发次数
  handleInput = debounce(
    (e, type) => {
      this.setState({
        [type]: e.detail.value,
      });
    },
    100,
    true,
  );

  handleLogin() {
    const { userPhone, password, nickName } = this.state;
    console.log(userPhone, password, nickName);

    if (!userPhone || !password || !nickName) {
      return tools.showToast("所有内容必须填写完整!");
    }

    const reg = /^1[3-9]\d{9}$/;

    if (!reg.test(userPhone)) {
      return tools.showToast("请填写正确格式的手机号");
    }

    tools.showLoading();

    loginRequest({
      userPhone,
      password,
      nickName,
    })
      .then((res) => {
        const { data } = res;

        tools.setStorageSyncExpires(
          "userInfo",
          {
            userPhone: data.userPhone,
            nickName: data.nickName,
          },
          60 * 60 * 2,
        );

        this.props.dispatch({
          type: "user/updateState",
          payload: {
            isLogin: !!data.userPhone,
            userPhone: data.userPhone,
            nickName: data.nickName,
          },
        });

        Taro.navigateBack();
      })
      .catch((err) => {
        const { data } = err;
        if (data?.message) {
          return tools.showToast(data.message);
        }
      })
      .finally(() => {
        tools.hideLoading();
      });
  }

  render() {
    return (
      <View className="login-container">
        <View className="top">你还知道登陆啊！</View>
        <View className="content">
          <Input
            type="text"
            placeholder="请输入昵称"
            className="nick-name"
            onInput={(e) => this.handleInput(e, "nickName")}
          ></Input>
          <Input
            type="text"
            placeholder="请输入手机号"
            className="user-phone"
            onInput={(e) => this.handleInput(e, "userPhone")}
          ></Input>
          <Input
            type="password"
            placeholder="请输入密码"
            className="password"
            onInput={(e) => this.handleInput(e, "password")}
          ></Input>
        </View>
        <View className="bottom">
          <AtButton
            type="primary"
            size="normal"
            circle
            className="login-btn"
            onClick={this.handleLogin.bind(this)}
          >
            登录
          </AtButton>
        </View>
      </View>
    );
  }
}
