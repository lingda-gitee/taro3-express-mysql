import { PureComponent } from "react";
import { View, Text, ScrollView } from "@tarojs/components";
import "./airportList.scss";

import { airportCityListRequest } from "@/common/api";
import tools from "@/utils/tools";
import { ERR_MSG } from "@/common/constant";

import CityItem from "./components/CityItem/index";

export default class Main extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      cityListObj: {},
      letterList: [], // 字母列表
      curLetter: "A", // 当前选中的字母, 一旦和DOM的id属性一致了, 会立刻滚动到该DOM处
    };
  }

  getCityList() {
    tools.showLoading();

    const flightCityList = tools.getStorageSyncExpires("flightCityList");

    if (flightCityList?.length) {
      const obj = this.formatList(flightCityList);
      this.setState({
        cityListObj: obj,
        letterList: Object.keys(obj),
      });

      tools.hideLoading();

      return;
    }

    airportCityListRequest()
      .then((res) => {
        console.log("res: ", res);
        const { data } = res;

        // 备忘录, 先做缓存
        tools.setStorageSyncExpires("flightCityList", data, 3000);

        const obj = this.formatList(data);
        this.setState({
          cityListObj: obj,
          letterList: Object.keys(obj),
        });
      })
      .catch(() => {
        tools.showToast(ERR_MSG);
      })
      .finally(() => {
        tools.hideLoading();
      });
  }

  // 把列表数据按照首字母分成若干个数组
  formatList(list) {
    const obj = {};
    if (list?.length) {
      list.map((ele) => {
        const { firstLetter } = ele;
        if (!obj[firstLetter]) {
          obj[firstLetter] = [];
        }

        obj[firstLetter].push(ele);
      });
    }

    return obj;
  }

  handleLetterClick(label) {
    this.setState({
      ...this.state,
      curLetter: label,
    });
  }

  async componentDidMount() {
    await this.getCityList();
  }

  render() {
    const { cityListObj, letterList, curLetter } = this.state;
    console.log(cityListObj, letterList);

    return (
      <View className="airport-list-container">
        <ScrollView
          scrollY
          scrollWithAnimation={tools.isAliPay ? false : true}
          style={{ height: "100vh" }}
          scrollIntoView={curLetter}
        >
          {letterList?.map((letter) => {
            const cityList = cityListObj[letter];

            return (
              <CityItem
                cityList={cityList}
                label={letter}
                key={letter}
              ></CityItem>
            );
          })}
        </ScrollView>
        <View className="letter-container">
          {letterList?.map((item) => {
            return (
              <View
                key={item}
                className="letter-item"
                onClick={this.handleLetterClick.bind(this, item)}
              >
                {item}
              </View>
            );
          })}
        </View>
      </View>
    );
  }
}
