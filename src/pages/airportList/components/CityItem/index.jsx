import { PureComponent } from "react";
import { View, Text } from "@tarojs/components";
import "./index.scss";
import Taro from "@tarojs/taro";

import { connect } from "react-redux";

// 连接组件和model
@connect(({ flightIndex }) => {
  return { ...flightIndex }; // 把state中所有属性挂载到组件的props(return的对象)上
})
export default class CityItem extends PureComponent {
  constructor(props) {
    super(props);
  }

  handleCityItemClick(cityInfo) {
    const { cityType } = this.props;
    const { cityId, cityName, airportName } = cityInfo;

    // 修改state
    this.props.dispatch({
      type: "flightIndex/updateState",
      payload:
        cityType === "depart"
          ? {
              dptCityId: cityId,
              dptAirportname: airportName,
              dptCityName: cityName,
            }
          : {
              arrCityId: cityId,
              arrAirportName: airportName,
              arrCityName: cityName,
            },
    });

    Taro.navigateBack(); // 回到上一页(首页)
  }

  render() {
    // cityList为单字母所包含的城市集合, label为单字母
    const { cityList, label } = this.props;

    return (
      <View className="city-item" id={label}>
        <Text className="label">{label}</Text>
        {cityList?.map((item) => {
          return (
            <View
              key={item.id}
              className="name"
              onClick={this.handleCityItemClick.bind(this, item)}
            >
              {`${item.cityName} - ${item.airportName}`}
            </View>
          );
        })}
      </View>
    );
  }
}
