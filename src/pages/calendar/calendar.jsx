import { PureComponent } from "react";
import { View, Text } from "@tarojs/components";
import { AtCalendar } from "taro-ui";
import "./calendar.scss";
import Taro from "@tarojs/taro";

import { connect } from "react-redux";

import { MIN_DATE, MAX_DATE } from "@/common/constant";

@connect(({ flightIndex }) => {
  return {
    flightIndex,
  };
})
export default class Calendar extends PureComponent {
  constructor(props) {
    super(props);
  }

  handleDateSelect(e) {
    const { start, end } = e.value;
    const { dispatch } = this.props;
    dispatch({
      type: "flightIndex/updateState",
      payload: {
        dptDate: start,
      },
    });

    Taro.navigateBack();
  }

  render() {
    const { dptDate } = this.props.flightIndex;

    return (
      <View className="calendar-container">
        <AtCalendar
          currentDate={dptDate}
          minDate={MIN_DATE}
          maxDate={MAX_DATE}
          onSelectDate={this.handleDateSelect.bind(this)}
        ></AtCalendar>
      </View>
    );
  }
}
