/**
 *
 * @param options 配置项
 * @returns 匿名函数, 该函数所返回的组件就是经过修饰(增加属性/方法, 传额外的props)的组件;
 */
const withShare = (options) => {
  // 参数为修饰的页面组件, TS装饰器的参数为obj key descriptor
  return function (WrapperComponent) {
    return class MyComponent extends WrapperComponent {
      // 增加的方法
      onShareAppMessage() {
        console.log("props: ", this.props);
        return {
          ...options,
          path: `/${this.props.tid}`,
        };
      }
    };
  };
};

export default withShare;
