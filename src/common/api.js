import tools from "@/utils/tools";

const BASE_URL = tools.isH5 ? "" : "http://192.168.0.100:3000"; // 上线后本地服务器必须使用ip, 不能使用localhost

// 首页广告接口
export const adsRequest = (data) =>
  tools.apiRequest({
    url: `${BASE_URL}/ads/advertising`,
    params: data,
  });

// 城市列表接口
export const airportCityListRequest = (data) => {
  return tools.apiRequest({
    url: `${BASE_URL}/city/airportList`,
    params: data,
  });
};

// 航班列表接口
export const flightListRequest = (data) => {
  return tools.apiRequest({
    url: `${BASE_URL}/list/singleList`,
    params: data,
  });
};

// 登陆接口
export const loginRequest = (data) => {
  return tools.apiRequest({
    url: `${BASE_URL}/login`,
    params: data,
    method: "POST",
  });
};

// 创建订单
export const orderRequest = (data) => {
  return tools.apiRequest({
    url: `${BASE_URL}/order/order`,
    params: data,
    method: "POST",
  });
};

// 查询订单接口
export const getOrderListRequest = (data) => {
  return tools.apiRequest({
    url: `${BASE_URL}/order/orderList`,
    params: data,
    method: "POST",
  });
};
