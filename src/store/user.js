import Taro from "@tarojs/taro";
import tools from "@/utils/tools";

const init = () => {
  const userInfo = tools.getStorageSyncExpires("userInfo");

  return {
    isLogin: !!userInfo?.userPhone,
    userPhone: userInfo?.userPhone,
    nickName: userInfo?.nickName,
  };
};

export default {
  namespace: "user",
  state: {
    ...init(),
  },
  reducers: {
    updateState(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    logout(state, action) {
      return {
        ...init(), // 均为undefined
      };
    },
  },
};
