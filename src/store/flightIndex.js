import dayjs from "dayjs";

const INIT_STATE = {
  dptCityId: 2,
  dptCityName: "上海", // 初始的出发城市
  dptAirportName: "虹桥机场",
  arrCityId: 1,
  arrCityName: "北京", // 初始的到达城市
  arrAirportName: "大兴机场",
  cityType: "depart", // 选择城市类型, depart出发, arrive到达
  dptDate: dayjs().format("YYYY-MM-DD"), // 起飞时间
};

export default {
  namespace: "flightIndex",
  state: {
    ...INIT_STATE,
  },
  // 同步更改state
  reducers: {
    updateState(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
  // 异步更改state
  effects: {},
};
