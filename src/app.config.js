export default defineAppConfig({
  // 主包
  pages: [
    "pages/index/index",
    "pages/order/order",
    "pages/airportList/airportList",
    "pages/calendar/calendar",
    // "pages/flight/list/list",
    // "pages/flight/detail/detail",
    "pages/login/login",
  ],
  // 分包
  subPackages: [
    {
      root: "pages/flight",
      // 不参与首屏加载的路由
      pages: ["list/list", "detail/detail"],
    },
  ],
  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#0066e6",
    navigationBarTitleText: "我的订单",
    navigationBarTextStyle: "black",
  },
  tabBar: {
    color: "#7E8389",
    selectedColor: "#5495e6",
    borderStyle: "black",
    backgroundColor: "#fff",
    list: [
      {
        pagePath: "pages/index/index",
        iconPath: "assets/imgs/home.png",
        selectedIconPath: "assets/imgs/home_active.png",
        text: "首页",
      },
      {
        pagePath: "pages/order/order",
        iconPath: "assets/imgs/order.png",
        selectedIconPath: "assets/imgs/order_active.png",
        text: "订单",
      },
    ],
  },
  permission: {
    "scope.userLocation": {
      desc: "为了更好的服务体验, 我们希望获取你的位置",
    },
  },
  requiredPrivateInfos: ["getLocation"],
});
