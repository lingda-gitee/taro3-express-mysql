import { Component } from "react";
import "./app.scss";
import createApp from "./dva";
import models from "./store";
import { Provider } from "react-redux";

import "promise-prototype-finally";

const dvaApp = createApp({
  initialState: {},
  models,
});

const store = dvaApp.getStore();

class App extends Component {
  componentDidMount() {}

  componentDidShow() {}

  componentDidHide() {}

  render() {
    // this.props.children 是将要会渲染的页面
    return <Provider store={store}>{this.props.children}</Provider>;
  }
}

export default App;
