import { create } from "dva-core";

let app; // dva实例
let store;
let dispatch;

const createApp = (options) => {
  app = create(options);
  if (!global.registered) {
    options.models.forEach((model) => {
      app.model(model); // 注册每个model
    });
  }
  global.registered = true;

  app.start();
  store = app._store;

  app.getStore = () => store; // 返回store, 以后直接调用app.getStore创建store
  dispatch = store.dispatch;
  app.dispatch = dispatch;

  return app;
};

export default createApp;
