import { PureComponent } from "react";
import { View, Text, Swiper, ScrollView, Block } from "@tarojs/components";
import Taro from "@tarojs/taro";

/**
 * @param {Array} list 列表数据
 * @param {Number} segmentNum 自定义分段的数量, 默认为10
 * @param {Object} scrollViewProps scrollView的参数
 * @param {Function} onComplete 二维数组加载完成的回调
 * @param {Function} onRender 二维数组每个item渲染完成的回调
 * @param {Function} onRenderBottom 二维数组下部分内容渲染完成的回调
 */

export default class VirtualList extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      twoDimenList: [], // 当前分段的二维数组, 该维度里面有n个元素(n个一维数组)
      isComplete: false, // 所有数据是否加载完成
    };
    this.initList = []; // 初始化的二维数组, 每个一维数组中都有seqmentNum条数据
  }

  handleComplete = () => {
    const { onComplete } = this.props;

    this.setState(
      {
        isComplete: true,
      },
      () => {
        onComplete?.();
      },
    );
  };

  formatList = (list = []) => {
    const { segmentNum } = this.props;
    let arr = []; // 一个纬度承载的数据(segmentNum条数据)
    let _list = []; // 二维数组

    list.forEach((item, index) => {
      arr.push(item);

      if ((index + 1) % segmentNum === 0) {
        // 已分割完一个纬度的数组
        _list.push(arr);
        arr = [];
      }
    });

    // 将分段不足一个纬度的剩余数据放进_list
    const restList = list.slice(_list.length * segmentNum); // 假设共51条, 分了5段还剩1个, 5 * 10 = 50下标开始即为rest元素
    if (restList?.length) {
      _list.push(restList);

      if (_list.length <= 1) {
        // 不够分一段
        this.handleComplete();
      }
    }

    this.initList = _list;
    this.setState({
      twoDimenList: this.initList.slice(0, 1),
    });
  };

  componentDidMount() {
    const { list } = this.props;
    this.formatList(list);
  }

  render() {
    const { twoDimenList, isComplete } = this.state;

    const { segmentNum, scrollViewProps, onRender } = this.props;

    return (
      <ScrollView
        scrollY
        id="zt-virtual-list"
        style={{ height: "100%" }}
        className="zt-virtual-list-container"
        {...scrollViewProps}
      >
        <View className="zt-main-list">
          {twoDimenList?.map((item, pageIdx) => {
            return (
              <View key={pageIdx} className={`wrap_${pageIdx}`}>
                <Block>
                  {item.map((ele, index) => {
                    return onRender?.(
                      ele,
                      pageIdx * segmentNum + index, // 每一条json的下标
                      pageIdx,
                    );
                  })}
                </Block>
              </View>
            );
          })}
        </View>
        {/* 虚拟列表下方加内容 */}
        {isComplete && onRenderBottom?.()}
      </ScrollView>
    );
  }
}

VirtualList.defaultProps = {
  list: [],
  segmentNum: 0,
};
