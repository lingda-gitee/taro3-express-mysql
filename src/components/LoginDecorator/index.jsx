import { Component } from "react";
import tools from "@/utils/tools";

// 给页面组件加入额外的方法/做额外的事情
function IsLogin(WrapperComponent) {
  return class extends Component {
    componentDidMount() {
      const storage = tools.getStorageSyncExpires("userInfo");

      if (!storage?.userPhone) {
        tools.navigateTo({
          url: "/pages/login/login",
        });
      }
    }

    render() {
      return <WrapperComponent />;
    }
  };
}

export default IsLogin;
