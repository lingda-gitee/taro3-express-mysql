import { PureComponent } from "react";
import { View, Text, Swiper } from "@tarojs/components";
import "./index.scss";

export default class Tab extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      curTabId: 0,
    };
  }

  handleTabItemClick(id) {
    if (id !== this.state.curTabId) {
      this.setState({
        curTabId: id,
      });

      this.props.onTabClick(id);
    }
  }

  // 监听swiper滑动
  handleSwiperChange(e) {
    const id = e.detail.current;
    this.setState(
      {
        curTabId: id,
      },
      () => {
        // 把id传递到外部
        this.props.onChange?.(e);
      },
    );
  }

  componentDidMount() {
    const { initTabId, tabList } = this.props;
    if (!initTabId) {
      this.setState({
        curTabId: tabList?.[0]?.id,
      });
    } else {
      this.setState({
        curTabId: initTabId,
      });
    }
  }

  render() {
    const { className, tabList, children, isOrder } = this.props;
    const { curTabId } = this.state;

    return (
      <View className={["tab-container", ` ${className}`].join("")}>
        <View className="tab-bar">
          {tabList &&
            tabList.map((item) => {
              return (
                <View
                  className={[
                    "tab-item",
                    curTabId === item.id ? " active" : "",
                  ].join("")}
                  key={item.id}
                  onClick={this.handleTabItemClick.bind(this, item.id)}
                >
                  {item.label}
                </View>
              );
            })}
        </View>
        <Swiper
          current={curTabId}
          className={["tab-content", isOrder ? " order" : ""].join("")}
          onChange={this.handleSwiperChange.bind(this)}
        >
          {/* current为0则渲染第一个Swiper-item, children表示给Tab插入的中间内容 */}
          {children}
        </Swiper>
      </View>
    );
  }
}
