const express = require("express");
const app = express();

const models = require("./model/index");

app.use(express.json()); // 解析请求体到req.body上, application/json
app.use(
  express.urlencoded({
    // application/x-www-form-urlencoded
    extended: false,
  }),
);

models(app);

const port = 3000;
app.listen(port, () => {
  console.log(`server listening on ${port}`);
});
