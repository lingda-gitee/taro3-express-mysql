const express = require("express");
const router = express.Router();
const sqlQuery = require("../mysql");
const request = require("request");

// request(
//   "https://www.brown77.cn/city/airportList",
//   { json: true },
//   async (err, res, body) => {
//     const strSql1 = `
//       create table airport_list (
//         id int not null auto_increment,
//         cityName char(50) not null,
//         cityId int not null,
//         firstLetter char(50) not null,
//         airportName char(50) not null,
//         primary key (id)
//       ) engine=innodb
//     `;
//     await sqlQuery(`drop table if exists airport_list`); // 存在就删除这个表

//     await sqlQuery(strSql1);

//     for (let i = 0; i < body.result.length; i++) {
//       const { id, cityId, cityName, firstLetter, airportName } = body.result[i];
//       const strSql2 = `insert into airport_list(id, cityName, cityId, firstLetter, airportName) values(${id}, '${cityName}', ${cityId}, '${firstLetter}', '${airportName}');`;

//       await sqlQuery(strSql2);
//     }
//   },
// );

router.get("/airportList", async (req, res) => {
  const strSql = `select * from airport_list`;
  try {
    const result = await sqlQuery(strSql);

    // 按首字母排序
    if (Array.isArray(result) && result.length > 0) {
      result.sort((x, y) => {
        if (x.firstLetter < y.firstLetter) {
          return -1; // 如果前一个元素的firstLetter值(ASCII码排序), 后一个元素就排在前一个元素前面
        } else if (x.firstLetter > y.firstLetter) {
          return 1;
        }
        return 0;
      });
    }

    res.send({
      code: 1,
      message: "请求成功",
      data: result,
    });
  } catch (err) {
    res.send({
      code: -1,
      message: "请求失败",
    });
  }
});

module.exports = router;
