const express = require("express");
const router = express.Router();
const sqlQuery = require("../mysql");
const request = require("request");
const dayjs = require("dayjs");

router.post("/login", async (req, res) => {
  try {
    const { userPhone, password, nickName } = req.body;

    const createTableSql = `
      create table if not exists user (
        id int auto_increment,
        userPhone char(11) not null,
        password char(10) not null,
        nickName char(50) not null,
        primary key (id)
      ) engine=innodb;
    `;

    await sqlQuery(createTableSql);

    // 查询表
    const sqlStr = `select userPhone from user where userPhone=${userPhone}`;
    const result = await sqlQuery(sqlStr);

    if (result.length) {
      const userInfo = `select nickName,password from user where userPhone=${userPhone}`;

      const userInfoRes = await sqlQuery(userInfo);

      if (userInfoRes.length && userInfoRes[0].password === password) {
        // 登录成功
        const curUser = userInfoRes[0];

        if (nickName !== curUser.nickName) {
          const updateSql = `update user set nickName='${nickName}' where userPhone=${userPhone}`;

          await sqlQuery(updateSql);
        }

        res.send({
          code: 1,
          message: "登录成功",
          data: {
            userPhone,
            nickName,
          },
        });
      } else {
        res.send({
          code: 2,
          message: "用户名或密码错误",
        });
      }
    } else {
      // 走注册流程
      const insertSql = `insert into user(id, nickName, userPhone, password) values (null, '${nickName}', '${userPhone}', '${password}')`;
      await sqlQuery(insertSql);

      res.send({
        code: 1,
        message: "注册并登陆成功",
        data: {
          userPhone,
          nickName,
        },
      });
    }
  } catch (err) {
    res.send({
      code: -1,
      message: "登录失败",
      data: err,
    });
  }
});

module.exports = router;
