const express = require("express");
const router = express.Router();
const sqlQuery = require("../mysql");
const request = require("request");
const dayjs = require("dayjs");

const randomPrice = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);

  return Math.floor(Math.random() * (max - min) + min);
};

router.get("/singleList", async (req, res) => {
  console.log("进来了");
  const { dptDate, dptAirportName, dptCityName, arrCityName, arrAirportName } =
    req.query;

  const strSql = `select * from airport_list`;

  try {
    const result = await sqlQuery(strSql);

    const resultList = result.map((item) => {
      return {
        ...item,
        dptTime: dptDate,
        dptAirportName,
        dptCityName,
        arrCityName,
        arrAirportName,
        price: randomPrice(300, 1000),
        dptTimeStr: dayjs(item.dptTime).format("HH:mm"),
        arrTimeStr: dayjs(item.arrTime).format("HH:mm"),
      };
    });

    res.send({
      code: 1,
      message: "请求成功",
      data: resultList,
    });
  } catch (err) {
    res.send({
      code: -1,
      message: "请求失败",
    });
  }
});

module.exports = router;
