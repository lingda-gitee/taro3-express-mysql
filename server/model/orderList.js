const express = require("express");
const router = express.Router();
const sqlQuery = require("../mysql");
const request = require("request");
const dayjs = require("dayjs");

router.post("/order", async (req, res) => {
  try {
    const { userPhone, orderInfo } = req.body;
    console.log("userPhone: ", userPhone);
    console.log("orderInfo", orderInfo);

    const { dptCityName, arrCityName, dptTimeStr, dptTime, price } = orderInfo;

    const createTableSql = `
      create table if not exists order_list (
        id int auto_increment,
        userPhone char(11) not null,
        dptCityName char(50) not null,
        arrCityName char(50) not null,
        dptTimeStr char(50) not null,
        dptTime char(50) not null,
        price decimal not null,
        primary key (id)
      ) engine=innodb;
    `; // 最后一个属性primary key (id), 后面一定不能加逗号

    await sqlQuery(createTableSql);

    const insertSql = `insert into order_list(id, userPhone, dptCityName, arrCityName, dptTimeStr, dptTime, price) values
    (null, '${userPhone}', '${dptCityName}', '${arrCityName}', '${dptTimeStr}', '${dptTime}', '${price}')`;

    await sqlQuery(insertSql);

    res.send({
      code: 1,
      message: "请求成功",
    });
  } catch (err) {
    res.send({
      code: -1,
      message: "请求失败",
    });
  }
});

router.post("/orderList", async (req, res) => {
  try {
    const { userPhone } = req.body;

    const querySql = `select * from order_list where userPhone=${userPhone} order by id desc`;

    const result = await sqlQuery(querySql);

    res.send({
      code: 1,
      message: "查询成功",
      data: result,
    });
  } catch (err) {
    res.send({
      code: -1,
      message: "请求失败",
      data: err,
    });
  }
});

module.exports = router;
