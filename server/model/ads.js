const express = require("express");
const router = express.Router();

const sqlQuery = require("../mysql");

const imgList = [
  "https://p.51credit.com/bbs/attachment/forum/app/2021/02/03/08E2B0AF23DF45709434BDF56D5E7264.jpg",
  "https://img2.baidu.com/it/u=3190820425,1866029002&fm=253&fmt=auto&app=138&f=JPEG?w=499&h=146",
];

// try {
//   // 创建表并查询的方法
//   const createTable = async () => {
//     // 建表sql语句
//     const createTableSql = `
//       create table if not exists ads (
//         id int auto_increment,
//         imgUrl char(255) not null,
//         primary key (id)
//       ) engine=innodb;
//     `;

//     await sqlQuery(createTableSql);
//     for (let i = 0; i < imgList.length; i++) {
//       const insertSql = `insert into ads(id, imgUrl) values(null, '${imgList[i]}')`; // 插入表sql语句
//       await sqlQuery(insertSql);
//     }
//   };

//   createTable();
// } catch (err) {}

router.get("/advertising", async (req, res) => {
  const strSql = `select * from ads`;
  try {
    const result = await sqlQuery(strSql);
    res.send({
      code: 1,
      message: "请求成功",
      data: result,
    });
  } catch (err) {
    res.send({
      code: -1,
      message: "请求失败",
    });
  }
});

module.exports = router;
