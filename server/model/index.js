// 接口汇总, use所有接口
module.exports = (app) => {
  app.use("/ads", require("./ads")); // /ads/advertising
  app.use("/city", require("./airportList"));
  app.use("/list", require("./list"));
  app.use(require("./login"));
  app.use("/order", require("./orderList"));
};
