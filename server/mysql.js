const mysql = require("mysql2");

const options = {
  host: "127.0.0.1",
  port: 3306,
  user: "root",
  password: "wc980428",
  database: "yuanfang",
};

const connection = mysql.createConnection(options);

connection.connect((err) => {
  if (err) {
    console.log("err: ", err);
    return;
  }
  console.log("数据库连接成功");
});

// connection.query("SELECT * from teacher_table", (err, res, fields) => {
//   console.log("查询结果: ", res);
// });

/**
 * @mean 封装公共的查询方法
 * @param {*} strSql 查询语句
 * @returns
 */
const sqlQuery = (strSql) => {
  return new Promise((resolve, reject) => {
    connection.query(strSql, (err, res, fields) => {
      if (err) {
        reject(err);
        return;
      }
      resolve(res);
    });
  });
};

module.exports = sqlQuery;
